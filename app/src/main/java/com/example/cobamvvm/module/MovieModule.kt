package com.example.cobamvvm.module

import com.example.cobamvvm.viewModel.MovieViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val movieModule = module {

    viewModel {
        MovieViewModel(get())
    }

}