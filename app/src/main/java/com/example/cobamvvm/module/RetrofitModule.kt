package com.example.cobamvvm.module

import com.example.cobamvvm.utils.Constants
import com.example.cobamvvm.utils.ServiceUtil
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json

import okhttp3.OkHttpClient
import okhttp3.MediaType.Companion.toMediaType
import org.koin.dsl.module
import retrofit2.Retrofit

val retrofitModule = module {    // 1

    single {   // 2
        okHttp()  // 3
    }
    single {
        retrofit(Constants.BASE_URL)  // 4
    }
    single {
        get<Retrofit>().create(ServiceUtil::class.java)   // 5
    }
}

private fun okHttp() = OkHttpClient.Builder()
    .build()
private fun retrofit(baseUrl: String) = Retrofit.Builder()
    .callFactory(OkHttpClient.Builder().build())
    .baseUrl(baseUrl)
    .addConverterFactory(Json.asConverterFactory("application/json".toMediaType()))  // 6
