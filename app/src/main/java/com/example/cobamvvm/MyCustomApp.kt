package com.example.cobamvvm

import android.app.Application
import com.example.cobamvvm.module.movieModule
import com.example.cobamvvm.module.picassoModule
import com.example.cobamvvm.module.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyCustomApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {   // 1
            androidLogger(Level.DEBUG)  // 2
            androidContext(this@MyCustomApp)  // 3
            modules(listOf(
                retrofitModule,
                picassoModule,
                movieModule
            ))  // 4
        }
    }
}